import { useState, useEffect } from 'react';

import { MainCard } from '../components/MainCard';
import { ContentBox } from '../components/ContentBox';
import { Header } from '../components/Header';
import { DateAndTime } from '../components/DateAndTime';
import { Select } from '../components/Select';
import { MetricsBox } from '../components/MetricsBox';
import { Contact } from '../components/Contact';
import { LoadingScreen } from '../components/LoadingScreen';

import styles from '../styles/Home.module.css';

const Home = () => {
    const [value, setValue] = useState([]);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const getData = async () => {
            const res = await fetch(
                'https://api.coindesk.com/v1/bpi/currentprice.json'
            );
            const data = await res.json();
            setValue(data.bpi);
            setLoading(true);
        };
        getData();
    }, []);

    return !loading ? (
        <LoadingScreen loadingMessage="Loading data..." />
    ) : (
        <div className={styles.container}>
            <div className={styles.wrapper}>
                <MainCard />
                <ContentBox>
                    <Header>
                        <DateAndTime />
                        <Select />
                    </Header>
                    <MetricsBox value={value} />
                    <Contact />
                </ContentBox>
            </div>
        </div>
    );
};

export default Home;
