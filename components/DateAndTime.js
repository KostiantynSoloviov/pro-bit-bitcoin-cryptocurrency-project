import styles from '../styles/DateAndTime.module.css';

export const DateAndTime = () => {
    const date = new Date();

    return (
        <div className={styles.wrapper}>
            <h2>{`Last update: ${date.toLocaleString()}`}</h2>
        </div>
    );
};
