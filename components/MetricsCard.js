import Image from 'next/image';
import styles from '../styles/MetricsCard.module.css';

export const MetricsCard = ({ title, iconSrc, rate, floatRate }) => {
    return (
        <div className={styles.wrapper}>
            <h1>{title}</h1>
            <div className={styles.image}>
                <Image
                    width="100px"
                    height="100px"
                    src={iconSrc}
                    alt="bitcoinIcon"
                />
            </div>
            <div className={styles.content}>
                <div>
                    <h2>Rate</h2>
                    <p>{rate}</p>
                    <h2>Float Rate</h2>
                    <p>{floatRate}</p>
                </div>
            </div>
        </div>
    );
};
