import { MetricsCard } from './MetricsCard';
import styles from '../styles/MetricsBox.module.css';

export const MetricsBox = ({ value }) => {
    return (
        <div className={styles.wrapper}>
            <MetricsCard
                title={value.USD.code}
                iconSrc={'/icons/usd.png'}
                rate={value.USD.rate}
                floatRate={value.USD.rate_float}
            />
            <MetricsCard
                title={value.GBP.code}
                iconSrc={'/icons/gbp.png'}
                rate={value.GBP.rate}
                floatRate={value.GBP.rate_float}
            />
            <MetricsCard
                title={value.EUR.code}
                iconSrc={'/icons/eur.png'}
                rate={value.EUR.rate}
                floatRate={value.EUR.rate_float}
            />
        </div>
    );
};
