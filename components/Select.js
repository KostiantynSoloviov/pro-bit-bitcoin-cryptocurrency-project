import React, { useState } from 'react';
import styles from '../styles/Select.module.css';

export const Select = () => {
    const [value, setValue] = useState('Bitcoin');

    return (
        <>
            <select
                className={styles.select}
                value={value}
                onChange={(e) => {
                    setValue(e.target.value);
                }}
            >
                <option value="Bitcoin">Bitcoin</option>
                <option value="Ethereum">Ethereum</option>
                <option value="Cronos">Cronos</option>
            </select>
        </>
    );
};
