import Image from 'next/image';
import styles from '../styles/MainCard.module.css';

export const MainCard = () => {
    return (
        <div className={styles.wrapper}>
            <h1 className={styles.nameCard}>{'Bitcoin'}</h1>
            <p className={styles.description}>{'(BTC)'}</p>
            <Image
                width="300px"
                height="300px"
                src={`/icons/bitcoin.svg`}
                alt="bitcoinIcon"
            />
        </div>
    );
};
