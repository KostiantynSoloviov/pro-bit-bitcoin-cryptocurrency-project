import Link from 'next/link';
import styles from '../styles/Contact.module.css';

export const Contact = () => {
    return (
        <div className={styles.wrapper}>
            <Link href="https://github.com/KostiantynSoloviov">
                <a className={styles.inactive}>Contact</a>
            </Link>
        </div>
    );
};
